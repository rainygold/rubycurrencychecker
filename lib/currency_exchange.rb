module CurrencyExchange

  # dependencies
  require 'json'
  require 'nori'
  require 'hashie'
  require_relative 'exceptions'

  # Return the exchange rate between from_currency and to_currency on date as a float.
  # Raises an exception if unable to calculate requested rate.
  # Raises an exception if there is no rate for the date provided.

  def self.rate(date, from_currency, to_currency)
    
    # filepath can be changed from JSON to XML
    # but data much be in the data dir
    parsed_file = data_parse('../data/eurofxref-hist-90d.json')
    
    # convert input date to a string
    date_as_str = date.to_s

    # first check for date
    date_check(date_as_str, parsed_file)

    # second check for currencies
    currency_check(date_as_str, parsed_file, from_currency, to_currency)

    # now calculate the cross-rate
    calculate_crossrate(date_as_str, parsed_file, from_currency, to_currency)

  end

  def self.data_parse(filepath)

    # read in the file here (saves on memory/performance)
    data_file = File.read(filepath)

    if filepath.include? "json"
      # convert to Ruby hash for generic access
      return parsed_file = JSON.parse(data_file)

    elsif filepath.include? "xml"
      parser = Nori.new
      parsed_data_file = parser.parse(data_file)

      # remove the root element that all xml conversations have
      parsed_data_file.extend Hashie::Extensions::DeepFind
      modified_data_file = parsed_data_file.deep_find("root")

      # fix any formatting problems / unwanted characters
      modified_data_file.keys.each {|key| modified_data_file[key[/[2][0-9]{3}\w[0-9]{2}\w[0-9]{2}/]] = modified_data_file.delete(key)}

      # xml conversions turn - into _ so change it back
      final_file = modified_data_file.each_with_object({}) {  |(key, value), final_file| final_file[key.gsub("_","-")] = value}
      return final_file
      
    else
      raise Exceptions::DataFileNotFound, "The filepath #{filepath} is invalid."
    end
  end

  # check for available dates
  def self.date_check(date, parsed)


    # get rid of any problematic strings
    date_modified_parsed = Hash[parsed.map {
                                  |key| [key.to_s.slice!(date)] }]

    # allows nested hash searching
    date_modified_parsed.extend Hashie::Extensions::DeepLocate

    # generic find method allows parsing of converted xml/json
    if (date_modified_parsed.deep_locate -> (key, value, object) { key == date}).any?
      return true
    # exception for incorrect input
    else
      raise Exceptions::DateNotFound, "The input date #{date} was not found."
    end
  end

  # check for available currencies
  def self.currency_check(date, parsed, currency_one, currency_two)

    # allows nested hash searching
    parsed.extend Hashie::Extensions::DeepFind

    # date has the first currency
    if parsed.deep_find(date).key?(currency_one) || currency_one == "EUR"

      # date has the second currency
      if parsed.deep_find(date).key?(currency_two) || currency_two == "EUR"
        return true

      # exception for missing currencies
      else
        raise Exceptions::CurrencyNotFound, "#{currency_two} was not found."
      end

    else
      raise Exceptions::CurrencyNotFound, "#{currency_one} was not found."
    end

  end

  # calculates the cross rate using a currency pair (their exchange rates)
  def self.calculate_crossrate(date, parsed, currency_one, currency_two)

    # allows nested hash searching
    parsed.extend Hashie::Extensions::DeepFetch
    
    # retrieve the individual rates
    if currency_one != "EUR"
      currency_one_rate = parsed.deep_fetch(date, currency_one).to_f
    else
      currency_one_rate = 1
    end
    if currency_two != "EUR"
      currency_two_rate = parsed.deep_fetch(date, currency_two).to_f
    else
      currency_two_rate = 1
    end

    # if euro was involved, change the equation(s)
    if currency_one_rate == 1
      crossrate = 1 / currency_two_rate * currency_one_rate  
      return crossrate.to_f
    end

    if currency_two_rate == 1
      crossrate = 1 / currency_one_rate * currency_two_rate
      return crossrate.to_f
    end

    # if no euro, then proceed with normal equation
    crossrate =  1 / currency_one_rate * currency_two_rate

    # result will always be a float
    return crossrate.to_f

  end

end
