module Exceptions

  # keep all custom exception logic/definitions in here!
  
  # dependencies

  class DateNotFound < StandardError
  end

  class CurrencyNotFound < StandardError
  end

  class DataFileNotFound < StandardError
  end

end
  
