# rubyCurrencyChecker - rainygold

## To run:

- bundle install
- in an irb session (with currency_exchange.rb loaded) call the methods

## Tools:

- OS: Ubuntu 18.10
- Editor: Emacs
- Ruby version: 2.6
- Testing: test-unit
- Version manager: RVM
- Gems: Hashie, Nokogiri, Nori, Json, test-unit

## Design

### lib

Main logic of the program was kept in currency_exchange.rb. The implementation of the solution was simple enough that splitting the logic into separate classes/files would have added unneeded complexity.

Custom extensions were defined in the exceptions.rb file. This allows the exceptions to be extended in the future and makes the program more maintainable.

If the data source has a different base currency than the provided, then the program should only require changing the 'EUR' references in currency_check and calculate_crossrate.

The last stage of the project was to generalise the data parsing process so that the only method that (in theory) should have to change to accommodate a different file type is data_parse.

### data

Untouched. The data was used for testing and during the general implementation of method logic. This was easily the aspect that created the most struggle. A requirement was to 'generify' the program because of the input (i.e. it may not be a JSON file) and there was an overreliance on the JSON during development.

An minor XML file was added for testing purposes.

As a last stage, a data_parse method was added to cover use-cases for XML / JSON data.

### test

In pursuit of simplicity, the test-unit gem was kept and used. Testing was focused on unit testing in order to check the behaviour of the individual methods involved in the process. Testing was kept to one file. If the program's logic was split into more classes/files then the tests would be split to match.

### Motivation

My Ruby experience is limited and this was a good reason to use the language again. My main project in the language was using the Rails framework for my MSc dissertation. It reminded me of the convenience of dynamic languages and how they make development more fun. During this small challenge I was mentally comparing a hypothetical Java equivalent solution, and I was happy to conclude that far more lines of code would be required in that language.

### Improvements that could be made

- The program could be more modular to accommodate easier changes to the code. E.g. the data-parse method could be split into a separate class and then you could simply instantiate a data_parse object with the appropriate file attribute such as xml/json or simply give it a direct piece of data.

- There are likely more idiomatic methods / gems to use for specific operations that I am not aware of at this time.

As it stands, the code is a monolith module, mostly because the project was a learning experience and that I have already invested some time into it. I would implement the above improvements if I started the project from scratch today.

