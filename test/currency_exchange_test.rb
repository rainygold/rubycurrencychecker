# http://test-unit.github.io/
require 'test/unit'
require 'currency_exchange'
require 'exceptions'
require 'date'

# Normally use the AAA method (arrange, act, assert)

class CurrencyExchangeTest < Test::Unit::TestCase

  # change the directory so tests can find data file
  def setup
    if !Dir.pwd.include? "lib"
      Dir.chdir("lib")
    end
  end

  #################################
  # Tests for CurrencyExchange.rate
  #################################

  def test_non_base_currency_exchange_is_correct
    correct_rate = 1.2870493690602498
    assert_equal correct_rate, CurrencyExchange.rate(Date.new(2018,11,22), "GBP", "USD")
  end

  # simple reverse of prior test
  def test_non_base_currency_reverse_exchange_is_correct
    correct_rate = 0.7769709725510829
    assert_equal correct_rate, CurrencyExchange.rate(Date.new(2018,11,22), "USD", "GBP")
  end

  def test_base_currency_exchange_is_correct
    correct_rate = 0.007763975155279502
    assert_equal correct_rate, CurrencyExchange.rate(Date.new(2018,11,22), "JPY", "EUR")
  end

  # simple reverse of prior test
  def test_base_currency_reverse_exchange_is_correct
    correct_rate = 0.007763975155279502
    assert_equal correct_rate, CurrencyExchange.rate(Date.new(2018,11,22), "EUR", "JPY")
  end

  #######################################
  # Tests for CurrencyExchange.date_check
  #######################################

  # these tests assume that a json file has been provided
  def test_existing_date_is_correct
    fake_json = '{"2018-12-11": {"fake1": 1}, "2018-12-12": {"fake2": 2}}'
    parsed_json = JSON.parse(fake_json)
    assert_true CurrencyExchange.date_check(Date.new(2018,12,11).to_s, parsed_json)
  end

  # check that the correct exception is raised when given an invalid date
  def test_non_existing_date_is_correct
    fake_json = '{"2018-12-11": {"fake1": 1}, "2018-12-12": {"fake2": 2}}'
    parsed_json = JSON.parse(fake_json)
    assert_raise(Exceptions::DateNotFound) do
      CurrencyExchange.date_check(Date.new(2016,12,11).to_s, parsed_json)
    end
  end

  ###########################################
  # Tests for CurrencyExchange.currency_check
  ###########################################

  # check that the currency exists in the dataset
  def test_existing_currency_json_is_correct
    fake_json = '{"2018-12-11": {"USD": 1, "CAN": 2}, "2018-12-12": {"CAN": 2}}'
    parsed_json = JSON.parse(fake_json)
    assert_true CurrencyExchange.currency_check(Date.new(2018,12,11).to_s, parsed_json, "USD", "CAN")
  end

  # check that the correct exception is raised
  def test_non_existing_currency_json_is_correct
    fake_json = '{"2018-12-11": {"USD": 1}, "2018-12-12": {"CAN": 2}}'
    parsed_json = JSON.parse(fake_json)
    assert_raise(Exceptions::CurrencyNotFound) do
      CurrencyExchange.currency_check(Date.new(2018,12,11).to_s, parsed_json, "USD", "JPY")
    end
  end

  # check that the currency exists in the dataset
  def test_existing_currency_xml_is_correct
    fake_xml =
'<root>
  <date-2018-12-11>
    <USD>1.121312</USD>
    <CAN>2.21312312</CAN>
  </date-2018-12-11>
  <date-2018-12-12>
    <USD>2.3123123</USD>
    <CAN>1.23123123</CAN>
  </date-2018-12-12>
</root>
'
    # repeat data parsing logic
    parser = Nori.new
    parsed_data_file = parser.parse(fake_xml)
    parsed_data_file.extend Hashie::Extensions::DeepFind
    modified_file = parsed_data_file.deep_find("root")
    modified_file.keys.each {|key| modified_file[key[/[2][0-9]{3}\w[0-9]{2}\w[0-9]{2}/]] = modified_file.delete(key)}
    final_file = modified_file.each_with_object({}) {  |(key, value), final_file| final_file[key.gsub("_","-")] = value}

    assert_true CurrencyExchange.currency_check(Date.new(2018,12,11).to_s, final_file, "USD", "CAN")
  end

  ################################################
  # Tests for CurrencyExchange.calculate_crossrate
  ################################################

  # rate method has been prior tested for the exact exchange rate, here we test for a correct output type (float)

  # check that a non-Euro (and existing) currency pair returns a rate
  def test_existing_currency_pair_is_correct
    fake_json = '{"2018-12-11": {"USD": 1.121312, "CAN": 2.21312312}, "2018-12-12": {"USD": 2.3123123, "CAN": 1.23123123}}'
    parsed_json = JSON.parse(fake_json)
    assert_true CurrencyExchange.calculate_crossrate(Date.new(2018,12,11).to_s, parsed_json, "USD", "CAN").is_a? Float
  end

  # check that a base currency pair returns a rate
  def test_base_currency_pair_is_correct
    fake_json = '{"2018-12-11": {"USD": 1.121312, "EUR": 2.21312312}, "2018-12-12": {"USD": 2.3123123, "EUR": 1.23123123}}'
    parsed_json = JSON.parse(fake_json)
    assert_true CurrencyExchange.calculate_crossrate(Date.new(2018,12,11).to_s, parsed_json, "USD", "EUR").is_a? Float
  end

  #######################################
  # Tests for CurrencyExchange.data_parse
  #######################################
  
  # test for checking XML parsing
  def test_xml_file_parsing_is_correct
    result = CurrencyExchange.data_parse("../data/eurofxref-hist-90d.xml")
    assert_true CurrencyExchange.date_check(Date.new(2018,12,11).to_s, result)
  end

  # test for checking JSON parsing
  def test_json_file_parsing_is_correct
    result = CurrencyExchange.data_parse ("../data/eurofxref-hist-90d.json")
    assert_true CurrencyExchange.date_check(Date.new(2018,12,11).to_s, result)
  end
  
end
